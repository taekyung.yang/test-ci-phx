# test-ci-phx

A project to test cicd with Phoenix's tutorial server.

testtest

## Setup

### Install Elixir with asdf
  
#### Create .tool-versions file

  $ asdf install


## Phoenix Guide's [hello] Project 

  https://hexdocs.pm/phoenix/up_and_running.html

  $ mix phx.new hello --database mysql

### Create docker-compose.yml file to setup Mysql

    hello/config/dev.exs - config :hello, Hello.Repo,

  $ docker-compose up

  $ mix phx.server

  

## Trobuleshooting

### GitLab push error

```
~/proj/gitlab/test-ci-phx git:(main) git push
remote:
remote: ========================================================================
remote:
remote: ERROR: You are not allowed to push code to this project.

remote:
remote: ========================================================================
remote:
fatal: Could not read from remote repository.
```

**solution**

`Add another public key` on the same machine and use that with 'personal' gitlab account (both on same machine).

https://stackoverflow.com/a/62218019
