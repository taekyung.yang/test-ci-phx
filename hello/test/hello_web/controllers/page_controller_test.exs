defmodule HelloWeb.PageControllerTest do
  use HelloWeb.ConnCase
  #
  # 各 testの実行には順序がない。
  #

  test "test-begin" do
    IO.puts("test begin")
    true
  end

  test "GET /", %{conn: conn} do
    IO.puts("test GET")
    conn = get(conn, "/")
    assert html_response(conn, 200) =~ "Welcome to Phoenix!"
  end

  test "test-end" do
    IO.puts("test end")
    true
  end
end
